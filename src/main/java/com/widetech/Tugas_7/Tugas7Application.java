package com.widetech.Tugas_7;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tugas7Application {

	public static void main(String[] args) {
		SpringApplication.run(Tugas7Application.class, args);		
	}

}

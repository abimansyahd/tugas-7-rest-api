package com.widetech.Tugas_7.controller;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.widetech.Tugas_7.domain.Product;
import com.widetech.Tugas_7.services.ProductService;

@RestController
@RequestMapping("/product")
public class ProductController {

	@Autowired
	ProductService pService;
	
	@GetMapping("/list")
	public List<Product> getAllProducts() {
	    return pService.findAll();
	}
	
	@GetMapping("/get/{id}")
	public ResponseEntity<Product> get(@PathVariable Integer id) {
	    try {
	        Product product = pService.findById(id);
	        return new ResponseEntity<Product>(product, HttpStatus.OK);
	    } catch (NoSuchElementException e) {
	        return new ResponseEntity<Product>(HttpStatus.NOT_FOUND);
	    }      
	}
	
	@PostMapping("/save")
	public ResponseEntity<String> saveProduct(@RequestBody Product product){
	pService.save(product);
	return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	@PutMapping("/edit/{id}")
	public ResponseEntity<Product> update(@RequestBody Product product, @PathVariable Integer id) {
	    try {
	        Product existProduct = pService.findById(id);
	        product.setId(existProduct.getId());
	        pService.save(product);
	        return new ResponseEntity<>(HttpStatus.OK);
	    } catch (NoSuchElementException e) {
	        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    }      
	}
	
	@DeleteMapping("/delete/{id}")
	public void delete(@PathVariable Integer id) {
	    pService.delete(id);
	}
	
	}
	


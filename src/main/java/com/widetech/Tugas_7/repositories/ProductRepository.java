package com.widetech.Tugas_7.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.widetech.Tugas_7.domain.Product;

public interface ProductRepository extends JpaRepository<Product, Integer>{
	public List<Product> findAll();
	public Optional<Product> findById(Integer id);
	
}

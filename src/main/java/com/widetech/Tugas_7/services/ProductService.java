package com.widetech.Tugas_7.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.widetech.Tugas_7.domain.Product;
import com.widetech.Tugas_7.repositories.ProductRepository;

@Service
public class ProductService {

	@Autowired
	ProductRepository repo;
	
	public List<Product> findAll() {
		return repo.findAll();		
	}
	
	@Transactional
	 public Product save(Product product) {
        return repo.save(product);
    }
	
	@Transactional
	public Product findById(Integer id) {
        return repo.findById(id).get();
    }
	
	@Transactional
	public void delete(Integer id) {
        repo.deleteById(id);
    }

	

	
     
}
